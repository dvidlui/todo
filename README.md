# Simple ToDo App by Swift

## Features
* User Sign-up
* User Log-in
* Task Management

## Back-end
* Rails-API
https://bitbucket.org/dvidlui/todo-rails-api
* Data Model
https://editor.ponyorm.com/user/david_lui/Todo

## CocoaPods
* AFNetworking
* RestKit
* Sliding Menu
* NSUserDefaults